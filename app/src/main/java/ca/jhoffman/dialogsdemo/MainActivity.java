package ca.jhoffman.dialogsdemo;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import ca.jhoffman.dialogsdemo.dialogs.CustomDialogFragment;
import ca.jhoffman.dialogsdemo.dialogs.ListDialogFragment;
import ca.jhoffman.dialogsdemo.dialogs.MultiChoiceListDialogFragment;
import ca.jhoffman.dialogsdemo.dialogs.PickerDialogFragment;
import ca.jhoffman.dialogsdemo.dialogs.SimpleDialogFragment;
import ca.jhoffman.dialogsdemo.dialogs.SingleChoiceListDialogFragment;

public class MainActivity   extends AppCompatActivity
                            implements  ListDialogFragment.ListItemListener,
                                        SingleChoiceListDialogFragment.SingleChoiceListItemListener,
                                        PickerDialogFragment.PickerDialogListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button simpleDialogButton = (Button) findViewById(R.id.activity_main_simple_dialog_button);
        simpleDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SimpleDialogFragment().show(getSupportFragmentManager(), "SimpleDialogFragment");
            }
        });

        Button listDialogButton = (Button) findViewById(R.id.activity_main_list_dialog_button);
        listDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ListDialogFragment().show(getSupportFragmentManager(), "ListDialogFragment");
            }
        });

        Button singleChoiceDialogButton = (Button) findViewById(R.id.activity_main_single_choice_list_dialog_button);
        singleChoiceDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SingleChoiceListDialogFragment().show(getSupportFragmentManager(), "SingleChoiceListDialogFragment");
            }
        });

        Button multiChoiceDialogButton = (Button) findViewById(R.id.activity_main_multi_choice_list_dialog_button);
        multiChoiceDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MultiChoiceListDialogFragment().show(getSupportFragmentManager(), "MultiChoiceListDialogFragment");
            }
        });

        Button pickerDialogButton = (Button) findViewById(R.id.activity_main_picker_dialog_button);
        pickerDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new PickerDialogFragment().show(getSupportFragmentManager(), "PickerDialogFragment");
            }
        });

        Button customDialogButton = (Button) findViewById(R.id.activity_main_custom_layout_dialog_button);
        customDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CustomDialogFragment().show(getSupportFragmentManager(), "CustomDialogFragment");
            }
        });
    }

    //
    // Listeners
    //

    @Override
    public void onListItemSelected(String item) {
        Toast.makeText(this, item, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSingleListItemSelected(String item) {
        Toast.makeText(this, item, Toast.LENGTH_LONG).show();
    }

    public void onMultiListItemsSelected(String items) {
        Toast.makeText(this, "From activity " +items, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemSelectedInPicker(String item) {
        Toast.makeText(this, item, Toast.LENGTH_LONG).show();
    }
}
