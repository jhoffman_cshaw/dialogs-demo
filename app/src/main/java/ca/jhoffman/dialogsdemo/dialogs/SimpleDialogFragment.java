package ca.jhoffman.dialogsdemo.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

/**
 * Created by jhoffman on 16-09-17.
 */
public class SimpleDialogFragment extends DialogFragment {
    String s = "_empty_";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            s = savedInstanceState.getString("DEMO_KEY", "instance, but no DEMO_KEY");
        } else {
            s = "savedInstanceState null, set default value";
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Simple dialog")
                .setMessage("Simple message")
                .setPositiveButton("Positive", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(getContext(), s, Toast.LENGTH_LONG).show();
                    }
                })
                .setNeutralButton("Neutral", null)
                .setNegativeButton("Negative", null);

        return builder.create();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("DEMO_KEY", "onSaveInstanceState");

        super.onSaveInstanceState(outState);
    }
}
