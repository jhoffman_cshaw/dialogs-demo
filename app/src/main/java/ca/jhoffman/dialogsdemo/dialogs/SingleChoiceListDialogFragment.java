package ca.jhoffman.dialogsdemo.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.widget.ArrayAdapter;
import android.widget.Toast;

/**
 * Created by jhoffman on 16-09-18.
 */
public class SingleChoiceListDialogFragment extends DialogFragment {

    private int selectedId;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        if (savedInstanceState != null) {
            selectedId = savedInstanceState.getInt("key_id", 0);
        } else {
            selectedId = 0; // default
        }

        final String[] items = new String[]{"Item 0", "Item 1", "Item 2", "Item 3", "Item 4"};
        ArrayAdapter<String> itemsAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_singlechoice, items);

        builder.setTitle("Single choice list dialog")
                .setSingleChoiceItems(itemsAdapter, selectedId, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getActivity(), "selected "+ which, Toast.LENGTH_SHORT).show();
                        selectedId = which;
                    }
                })
                .setPositiveButton("Apply", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ((SingleChoiceListItemListener)getActivity()).onSingleListItemSelected(items[selectedId]);
                    }
                });

        return builder.create();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("key_id", selectedId);

        super.onSaveInstanceState(outState);
    }

    //
    //Interface SingleChoiceListItemListener
    //

    public interface SingleChoiceListItemListener {
        void onSingleListItemSelected(String item);
    }
}
