package ca.jhoffman.dialogsdemo.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.widget.ArrayAdapter;

/**
 * Created by jhoffman on 16-09-18.
 */
public class ListDialogFragment extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final String[] items = new String[]{"Item 0", "Item 1", "Item 2", "Item 3", "Item 4"};
        ArrayAdapter<String> itemsAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_item, items);

        builder.setTitle("List dialog")
                .setAdapter(itemsAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ((ListItemListener)getActivity()).onListItemSelected(items[which]);
                    }
                })
                .setNegativeButton("Cancel", null);

        return builder.create();
    }

    //
    //Interface ListItemListener
    //

    public interface ListItemListener {
        void onListItemSelected(String item);
    }
}
