package ca.jhoffman.dialogsdemo.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import ca.jhoffman.dialogsdemo.R;

/**
 * Created by jhoffman on 16-09-18.
 */
public class CustomDialogFragment extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_custom, null);

        builder.setTitle("Custom dialog")
                .setView(view)
                .setPositiveButton("Positive", null)
                .setNeutralButton("Neutral", null)
                .setNegativeButton("Negative", null);

        view.findViewById(R.id.dialog_custom_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Button clicked!", Toast.LENGTH_LONG).show();
            }
        });

        return builder.create();
    }
}
