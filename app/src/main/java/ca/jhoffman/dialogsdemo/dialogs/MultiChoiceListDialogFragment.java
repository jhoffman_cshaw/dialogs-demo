package ca.jhoffman.dialogsdemo.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by jhoffman on 16-09-18.
 */
public class MultiChoiceListDialogFragment extends DialogFragment {

    private boolean[] selectedIds;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final String[] items = new String[]{"Item 0", "Item 1", "Item 2", "Item 3", "Item 4"};

        if (savedInstanceState != null) {
            selectedIds = savedInstanceState.getBooleanArray("key_ids");
        } else {
            selectedIds = new boolean[items.length]; // default all to false
        }

        builder.setTitle("Multi choice list dialog")
                .setMultiChoiceItems(items, selectedIds, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        Toast.makeText(getActivity(), "selected "+ which + ", "+ isChecked, Toast.LENGTH_SHORT).show();

                        selectedIds[which] = isChecked;
                    }
                })
                .setPositiveButton("Apply", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String s = "Selected: ";

                        for(int i = 0; i < selectedIds.length; i++) {
                            if (selectedIds[i] == true) {
                                s += (i + " ");
                            }
                        }

                        ((MultiChoiceListItemListener)getActivity()).onMultiListItemsSelected(s);
                    }
                });

        return builder.create();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBooleanArray("key_ids", selectedIds);

        super.onSaveInstanceState(outState);
    }

    //
    //Interface MultiChoiceListItemListener
    //

    public interface MultiChoiceListItemListener {
        void onMultiListItemsSelected(String item);
    }
}
